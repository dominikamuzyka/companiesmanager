﻿using System;

namespace CompaniesManager.Exceptions
{
    public class CompanyExistsException : Exception
    {
        public CompanyExistsException(string name)
            : base($"Company '{name}' already exists.")
        {
        }
    }
}
