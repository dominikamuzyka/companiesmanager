﻿using System;

namespace CompaniesManager.Exceptions
{
    public class CompanyNotFoundException : Exception
    {
        public CompanyNotFoundException(long id)
            : base($"Company with ID: '{id}' does not exist.")
        {
        }
    }
}
