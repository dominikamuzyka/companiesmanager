﻿using System;
using System.Collections.Generic;

namespace CompaniesManager.Exceptions
{
    public class EmployeesExistException : Exception
    {
        public IList<string> Employees { get; }

        public EmployeesExistException(IList<string> employees)
            : base($"Employees '[{string.Join(";", employees)}]' already exist in other company.")
        {
            Employees = employees;
        }
    }
}
