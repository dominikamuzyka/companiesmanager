﻿using CompaniesManager.DataAccessLayer.Models;

namespace CompaniesManager.DataAccessLayer.Extensions
{
    public static class EmployeeExtensions
    {
        public static bool AreEqual(this Employee employee, Employee toCompare)
        {
            return (employee.FirstName == toCompare.FirstName
                && employee.LastName == toCompare.LastName
                && employee.DateOfBirth == toCompare.DateOfBirth);
        }
    }
}
