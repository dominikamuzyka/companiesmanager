﻿using CompaniesManager.DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace CompaniesManager.DataAccessLayer
{
    public class CompaniesDbContext : DbContext
    {
        public DbSet<Company> Companies { get; set; }
        public DbSet<Employee> Employees { get; set; }

        public CompaniesDbContext(DbContextOptions<CompaniesDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Company>()
                .HasIndex(x => x.Name)
                .IsUnique();

            modelBuilder.Entity<Employee>()
                .HasIndex(nameof(Employee.FirstName), nameof(Employee.LastName), nameof(Employee.DateOfBirth))
                .IsUnique();

            modelBuilder.Entity<Employee>()
               .HasOne(x => x.Company)
               .WithMany(x => x.Employees);
        }

        public void DetachAllEntities()
        {
            var changedEntriesCopy = this.ChangeTracker.Entries()
                .Where(x => x.State == EntityState.Added
                            || x.State == EntityState.Modified
                            || x.State == EntityState.Deleted)
                .ToList();

            foreach (var entry in changedEntriesCopy)
            {
                entry.State = EntityState.Detached;
            }
        }
    }
}
