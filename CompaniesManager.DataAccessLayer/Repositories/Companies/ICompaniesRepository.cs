﻿using CompaniesManager.DataAccessLayer.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CompaniesManager.DataAccessLayer.Repositories
{
    /// <summary>
    /// Repository to manage Companies
    /// </summary>
    public interface ICompaniesRepository
    {
        /// <summary>
        /// Creates new Company accordingly to given object
        /// </summary>
        /// <param name="company">Company which should be created</param>
        /// <returns>Company ID</returns>
        Task<long> CreateCompanyAsync(Company company);

        /// <summary>
        /// Replaces Company using given in object Company ID
        /// </summary>
        /// <param name="company">Company which should be updated</param>
        Task<long> UpdateCompanyAsync(Company company);

        /// <summary>
        /// Removes Company for given Company ID
        /// </summary>
        /// <param name="id">Company ID</param>
        Task<long> DeleteCompanyAsync(long id);

        /// <summary>
        /// Search Companies using given criteria
        /// </summary>
        /// <param name="keyword">Matches Company.Name, Employee.FirtName or LastName</param>
        /// <param name="birthFrom">Employees date of birth from</param>
        /// <param name="birthTo">Employees date of birth to</param>
        /// <param name="jobTitle">Employess job title</param>
        /// <returns>List of Companies that match given parameters</returns>
        Task<List<Company>> GetCompaniesAsync(string keyword = "", DateTime? birthFrom = null, DateTime? birthTo = null, JobTitle? jobTitle = null);
    }
}
