﻿using CompaniesManager.DataAccessLayer.Extensions;
using CompaniesManager.DataAccessLayer.Models;
using CompaniesManager.Exceptions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CompaniesManager.DataAccessLayer.Repositories
{
    public class CompaniesRepository : BaseRepository, ICompaniesRepository
    {
        public CompaniesRepository(CompaniesDbContext context)
            : base(context)
        {
        }

        public async Task<long> CreateCompanyAsync(Company company)
        {
            _ = company ?? throw new ArgumentNullException(nameof(company));

            try
            {
                _context.Companies.Add(company);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                _context.DetachAllEntities();

                if (CompanyExists(company))
                {
                    throw new CompanyExistsException(company.Name);
                }

                List<string> existingEmployees = GetExistingEmployees(company.Employees);
                if (existingEmployees != null)
                {
                    throw new EmployeesExistException(existingEmployees);
                }

                throw;
            }
            catch (Exception)
            {
                throw;
            }

            return company.Id;
        }

        public async Task<long> UpdateCompanyAsync(Company company)
        {
            _ = company ?? throw new ArgumentNullException(nameof(company));

            Company existingCompany = await _context
                   .Companies
                   .Include(x => x.Employees)
                   .FirstOrDefaultAsync(x => x.Id == company.Id);

            _ = existingCompany ?? throw new CompanyNotFoundException(company.Id);

            try
            {
                _context.Entry(existingCompany).CurrentValues.SetValues(company);

                foreach (var existingEmployee in existingCompany.Employees.ToList())
                {
                    if (!company.Employees.Any(x => x.AreEqual(existingEmployee)))
                    {
                        _context.Employees.Remove(existingEmployee);
                    }
                }

                foreach (var employee in company.Employees)
                {
                    var existingChild = existingCompany.Employees
                        .Where(x => x.AreEqual(employee))
                        .SingleOrDefault();

                    if (existingChild == null)
                    {
                        existingCompany.Employees.Add(employee);
                    }
                }

                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                List<string> existingEmployees = GetExistingEmployees(company.Employees);
                if (existingEmployees != null)
                {
                    throw new EmployeesExistException(existingEmployees);
                }

                throw;
            }
            catch (Exception)
            {
                throw;
            }

            return company.Id;
        }

        public async Task<long> DeleteCompanyAsync(long id)
        {
            Company existingCompany = await _context
                   .Companies
                   .Include(x => x.Employees)
                   .FirstOrDefaultAsync(x => x.Id == id);

            _ = existingCompany ?? throw new CompanyNotFoundException(id);

            try
            {
                _context.Companies.Remove(existingCompany);
                await _context.SaveChangesAsync();
            }
            catch (Exception)
            {
                throw;
            }

            return id;
        }

        public async Task<List<Company>> GetCompaniesAsync(string keyword = null,
            DateTime? birthFrom = null,
            DateTime? birthTo = null,
            JobTitle? jobTitle = null)
        {
            List<Company> results = new List<Company>();

            birthFrom ??= DateTime.MinValue;
            birthTo ??= DateTime.MaxValue;
            
            List<JobTitle> jobTitles = new List<JobTitle>();

            if (jobTitle != null)
            {
                jobTitles.Add((JobTitle)jobTitle);
            }
            else
            {
                jobTitles.AddRange((IList<JobTitle>)Enum.GetValues(typeof(JobTitle)));
            }

            if (string.IsNullOrEmpty(keyword))
            {
                results = await _context.Companies
                    .Include(x => x.Employees)
                    .Where(x => x.Employees.Any(y => y.DateOfBirth >= birthFrom && y.DateOfBirth <= birthTo))
                    .Where(x => x.Employees.Any(y => jobTitles.Contains(y.JobTitle)))
                    .Select(x => x)
                    .ToListAsync();
            }
            else
            {
                results = await _context.Companies
                    .Include(x => x.Employees)
                    .Where(x => x.Name.Contains(keyword) || x.Employees.Any(y => y.FirstName.Contains(keyword) || y.LastName.Contains(keyword)))
                    .Where(x => x.Employees.Any(y => y.DateOfBirth >= birthFrom && y.DateOfBirth <= birthTo))
                    .Where(x => x.Employees.Any(y => jobTitles.Contains(y.JobTitle)))
                    .Select(x => x)
                    .ToListAsync();
            }

            return results;
        }

        private bool CompanyExists(Company company)
        {
            return _context.Companies.Any(x => x.Name == company.Name);
        }

        private List<string> GetExistingEmployees(IList<Employee> employees)
        {
            List<string> existingEmployess = new List<string>();

            foreach (var employee in employees)
            {
                if (_context.Employees.Count() > 0)
                {
                    //I should use here x.AreEqual(employee) but it throws exception which I do not undestand
                    var existingEmployee = _context.Employees
                        .Where(x => x.FirstName == employee.FirstName
                                    && x.LastName == employee.LastName
                                    && x.DateOfBirth == employee.DateOfBirth)
                        .FirstOrDefault();

                    if (existingEmployee != null)
                    {
                        existingEmployess.Add(existingEmployee.ToString());
                    }
                }
            }

            return existingEmployess;
        }
    }
}
