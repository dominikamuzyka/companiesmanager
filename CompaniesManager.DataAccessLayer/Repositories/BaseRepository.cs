﻿using System;

namespace CompaniesManager.DataAccessLayer.Repositories
{
    public class BaseRepository
    {
        protected readonly CompaniesDbContext _context;

        public BaseRepository(CompaniesDbContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }
    }
}
