﻿namespace CompaniesManager.DataAccessLayer.Models
{
    /// <summary>
    /// Represents Job Title
    /// </summary>
    public enum JobTitle
    {
        Administrator = 0,
        Developer,
        Architect,
        Manager,
    }
}