﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CompaniesManager.DataAccessLayer.Models
{
    /// <summary>
    /// Model represents Company
    /// </summary>
    public class Company
    {
        [Key]
        [Required]
        public long Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        [Range(1800, 2020)]
        public int EstablishmentYear { get; set; }

        public IList<Employee> Employees { get; set; }
    }
}
