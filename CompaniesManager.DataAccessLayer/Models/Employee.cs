﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.ComponentModel.DataAnnotations;

namespace CompaniesManager.DataAccessLayer.Models
{
    /// <summary>
    /// Model represents Employee
    /// </summary>
    public class Employee
    {
        [Key]
        [Required]
        public long Id { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public DateTime DateOfBirth { get; set; }

        [Required]
        [JsonConverter(typeof(StringEnumConverter))]
        public JobTitle JobTitle { get; set; }

        public Company Company { get; set; }

        public override string ToString()
        {
            return $"{FirstName} {LastName} {DateOfBirth.Day}-{DateOfBirth.Month}-{DateOfBirth.Year}";
        }
    }
}
