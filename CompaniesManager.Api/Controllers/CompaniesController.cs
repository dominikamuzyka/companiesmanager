﻿using CompaniesManager.Api.DataProxies;
using CompaniesManager.Api.RequestsModels;
using CompaniesManager.DataAccessLayer.Models;
using CompaniesManager.Exceptions;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace CompaniesManager.Api.Controllers
{
    [Route("api/company")]
    [ApiController]
    public class CompaniesController : ControllerBase
    {
        private readonly ICompaniesDataProxy companiesDataProxy;

        public CompaniesController(ICompaniesDataProxy companiesDataProxy)
        {
            this.companiesDataProxy = companiesDataProxy ?? throw new ArgumentNullException(nameof(companiesDataProxy));
        }

        //POST: api/company/create
        [HttpPost]
        [Route("create")]
        public async Task<ActionResult<long>> CreateCompany(Company company)
        {
            try
            {
                long id = await companiesDataProxy.CreateCompanyAsync(company);
                return StatusCode((int)HttpStatusCode.Created, new { id });
            }
            catch (CompanyExistsException)
            {
                return BadRequest($"Company '{company.Name}' already exists.");
            }
            catch (EmployeesExistException ex)
            {
                return BadRequest($"Employee/s '[{string.Join("; ", ex.Employees)}]' is/are hired in another Company.");
            }
            catch (Exception)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, "Unexpected server error.");
            }
        }

        //POST: api/company/search
        [HttpPost]
        [Route("search")]
        public async Task<ActionResult<IEnumerable<Company>>> SearchCompanies(SearchCompany searchCompanyParams)
        {
            try
            {
                List<Company> companies = await companiesDataProxy.SearchCompaniesAsync(searchCompanyParams);
                return Ok(new { Results = companies });
            }
            catch (Exception)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, "Unexpected server error.");
            }
        }

        //PUT: api/company/update/{id}
        [HttpPut]
        [Route("update/{id:long}")]
        public async Task<ActionResult> UpdateCompany(long id, Company company)
        {
            company.Id = id;

            try
            {
                await companiesDataProxy.UpdateCompanyAsync(company);
            }
            catch (CompanyNotFoundException)
            {
                return NotFound($"Company '{id}' does not exist.");
            }
            catch (EmployeesExistException ex)
            {
                return BadRequest($"Employee/s '[{string.Join("; ", ex.Employees)}]' is/are hired in another Company.");
            }
            catch (Exception)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, "Unexpected server error.");
            }

            return Ok();
        }

        //DELETE: api/company/delete/{id}
        [HttpDelete]
        [Route("delete/{id:long}")]
        public async Task<ActionResult> DeleteCompanyAsync(long id)
        {
            try
            {
                await companiesDataProxy.DeleteCompanyAsync(id);
            }
            catch (CompanyNotFoundException)
            {
                return NotFound($"Company '{id}' does not exist.");
            }
            catch (Exception)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, "Unexpected server error.");
            }

            return StatusCode((int)HttpStatusCode.Gone, new { });
        }
    }
}
