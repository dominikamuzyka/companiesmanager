﻿using CompaniesManager.DataAccessLayer.Models;
using System;

namespace CompaniesManager.Api.RequestsModels
{
    public class SearchCompany
    {
        public string Keyword { get; set; }
        public DateTime? EmployeeDateOfBirthFrom { get; set; }
        public DateTime? EmployeeDateOfBirthTo { get; set; }
        public JobTitle? EmployeeJobTitle { get; set; }
    }
}
