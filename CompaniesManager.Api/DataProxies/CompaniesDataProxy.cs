﻿using CompaniesManager.Api.RequestsModels;
using CompaniesManager.DataAccessLayer.Models;
using CompaniesManager.DataAccessLayer.Repositories;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CompaniesManager.Api.DataProxies
{
    public class CompaniesDataProxy : ICompaniesDataProxy
    {
        private readonly ICompaniesRepository companiesRepository;

        public CompaniesDataProxy(ICompaniesRepository companiesRepository)
        {
            this.companiesRepository = companiesRepository ?? throw new ArgumentNullException(nameof(companiesRepository));
        }

        public async Task<long> CreateCompanyAsync(Company company)
        {
            return await companiesRepository.CreateCompanyAsync(company);
        }

        public async Task<long> UpdateCompanyAsync(Company company)
        {
            await companiesRepository.UpdateCompanyAsync(company);
            return company.Id;
        }

        public async Task<List<Company>> SearchCompaniesAsync(SearchCompany searchCompanyParams)
        {
            return await companiesRepository.GetCompaniesAsync(searchCompanyParams.Keyword,
                searchCompanyParams.EmployeeDateOfBirthFrom,
                searchCompanyParams.EmployeeDateOfBirthTo,
                searchCompanyParams.EmployeeJobTitle);
        }

        public async Task<long> DeleteCompanyAsync(long id)
        {
            await companiesRepository.DeleteCompanyAsync(id);
            return id;
        }
    }
}
