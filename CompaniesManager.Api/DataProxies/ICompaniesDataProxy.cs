﻿using CompaniesManager.Api.RequestsModels;
using CompaniesManager.DataAccessLayer.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CompaniesManager.Api.DataProxies
{
    /// <summary>
    /// Data proxy enables to fecth and manage Companies data
    /// </summary>
    public interface ICompaniesDataProxy
    {
        /// <summary>
        /// Creates new Company
        /// </summary>
        /// <param name="company">Company to create</param>
        /// <returns>New Company ID</returns>
        Task<long> CreateCompanyAsync(Company company);

        /// <summary>
        /// Updates given Company
        /// </summary>
        /// <param name="company">Company to update</param>
        Task<long> UpdateCompanyAsync(Company company);

        /// <summary>
        /// Gets Companies using given criteria
        /// </summary>
        /// <param name="searchCompanyParams">Criteria for search</param>
        /// <returns>Collection of Companies</returns>
        Task<List<Company>> SearchCompaniesAsync(SearchCompany searchCompanyParams);

        /// <summary>
        /// Delete Company by given ID
        /// </summary>
        /// <param name="id">Company to delete</param>
        Task<long> DeleteCompanyAsync(long id);
       
    }
}
