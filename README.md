# Companies Manager #
This project provides API to manage companies.

## Informal use cases ##
User is able to:
 *  create company with given employees
 *  edit company with given all details to replace objects
 *  delete existing company
 *  search for companies using given criteria

## Project components ##
##### **Server side:**
* API .Net Core 3.1
* Entity Framework Core 3.1 

### Database structure ###
I created `Companies` and `Employees` with relation **one to many**.
To model these relations I used EF Core **Code First** approach. Corresponding models are `Company` and `Employee`.

### Database provider ###
For development purposes I used **LocalDB SQL Server** but to enable easy start of application on other machine/environment I changed provider to **SQLite**. It will create new SQLite DB file and save into it.

For presentation - I wanted to use `InMemory` but it does **not** use relational DB. 

### API endpoints ###
Accordingly to given guidelines.

## Run ##
### First approach
Download package from [my repo](https://bitbucket.org/dominikamuzyka/companiesmanager/downloads/) with compiled project. Run executable `CompaniesManager.Api.exe`. API is available on [localhost:5001/api/company](https://localhost:5001/api/company).

At first run - empty DB will be created automatically in `CompaniesDB.db` file.

### Second approach
Fetch code, compile and run on your machine.

Clone repository:
```git clone https://dominikamuzyka@bitbucket.org/dominikamuzyka/companiesmanager.git```

Open solution:
* `build` solution
* and `start`

API is available on [localhost:5001/api/company](https://localhost:5001/api/company).

## TODO ##
* add anonymous access to POST: api/company/search
* fix `SearchCompany` model to accept array of `JobTitle` not only one element and adjust changes in data access according to this change
* add unit tests
* implement middleware for request validation to use custom response format